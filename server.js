const express = require('express');
const app = express();
const swaggerUi = require('swagger-ui-express');
const passport = require('passport');
const flash = require('connect-flash');
const session = require('express-session');
var path = require('path');


require('dotenv').config();

// Passport Config
require('./config/passport-middleware')(passport);

// EJS
app.use(require('express-ejs-layouts'));
app.set('view engine', 'ejs');

// Mongoose
const db = require('mongoose');
db.connect(process.env.MONGOOSE_URI, { useNewUrlParser: true} )
    .then(() => console.log('Mongoose!!'))
    .catch(err => console.log(err));

// Express body parser
app.use(express.urlencoded({ extended: true }));

// Express session
app.use(
  session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
  })
);

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Connect flash
app.use(flash());

// Global variables
app.use(function(req, res, next) {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  next();
});

// Routes
app.use('/', require('./routes/index.js'));
app.use('/users', require('./routes/users.js'));
app.use('/swagger', swaggerUi.serve, require('./routes/swagger.js'));


app.listen(9001);