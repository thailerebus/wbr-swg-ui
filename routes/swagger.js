const express = require('express');
const swaggerUi = require('swagger-ui-express');
const router = express.Router();

const { ensureAuthenticated } = require('../config/auth-middleware');

var options = {
    explorer: true,
    swaggerOptions: {
      urls: [
        {
          url: 'http://127.0.0.1:1111/docs/swagger-WbGame-V1.yaml',
          name: 'Spec1'
        },
        {
          url: 'http://127.0.0.1:1111/docs/swagger-all.yaml',
          name: 'Spec2'
        }
      ]
    }
  }

// Swagger-UI
router.get('/api-docs', ensureAuthenticated, swaggerUi.setup(null, options));

module.exports = router;