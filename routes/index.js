const express = require('express');

const router = express.Router();
const { forwardAuthenticated } = require('../config/auth-middleware');


// Swagger
router.get('/', forwardAuthenticated, (req, res) => res.render('login'));

// // Home
// router.get('/home', ensureAuthenticated, (req, res) =>
//   res.render('home', {
//     user: req.user
//   })
// );

module.exports = router;